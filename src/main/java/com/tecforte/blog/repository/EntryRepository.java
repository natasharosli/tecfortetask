package com.tecforte.blog.repository;
import com.tecforte.blog.domain.Entry;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Entry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EntryRepository extends JpaRepository<Entry, Long> {

    @Query("delete from Entry entry where entry.blog.id = ?1 and entry.title like CONCAT('%',?2,''%')");
    List<Entry> findByBlogIdKeyword(id,keyword);

}
